# Flutter Intervie Assessment

I made the following assumptions:
1. That we are expected to do what we can under 8 hours and hence, I stopped on the 8 hour mark and rouunded up
2. That we are expected to use assets from the Figma file only, so I exported usable images
3. That I am expected to my preffered state-management
4. That despite the Image in the list tile and Walker Details having diffrent images, Clicking a walker on the homepage should go to details of the walker with their image and I've coded as such

## If I Had More Time
Given more time, I would have been able to write cleaner codes and appropriate widget and unit tests
