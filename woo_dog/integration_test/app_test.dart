import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:integration_test/integration_test.dart';
import 'package:woo_dog/main.dart' as app;

void main() {
  group('app Test', () {
    IntegrationTestWidgetsFlutterBinding.ensureInitialized();
    testWidgets('full app test', (tester) async {
      app.main();
      await tester.pumpAndSettle();
      //find Join our Community Button
      final moveToSignUp = find.byKey(const Key("Join Community"));
      await tester.pumpAndSettle();
      //tap Join out Community Button
      await tester.tap(moveToSignUp);

      //find all TextFields by Key
      final fullName = find.byKey(const Key("Full Name"));
      final email = find.byKey(const Key("Email"));
      final password = find.byKey(const Key("Password"));
      final signUp = find.byKey(const Key("Sign Up"));

      //pass inputs to textfields
      await tester.enterText(fullName, "Hassan Teslim");
      await tester.enterText(email, "hassanteslim12@gmail.com");
      await tester.enterText(password, "Test1234");
      await tester.pumpAndSettle();

      //tap sign up button
      await tester.tap(signUp);
      await tester.pumpAndSettle();

      //get to dashboard
    });
  });
}
