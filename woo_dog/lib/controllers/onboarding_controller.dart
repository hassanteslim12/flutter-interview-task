import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get/utils.dart';
import 'package:woo_dog/models/onboarding_model.dart';
import 'package:woo_dog/views/sign_up_view.dart';

class OnboardingController extends GetxController {
  var selectedPageindex = 0.obs;
  var pageController = PageController();
  //var pageViewAutoAnimateTimer;
  bool get isLastPage => selectedPageindex.value == onboardingItems.length - 1;
  //int index;

  List<OnboardingModel> onboardingItems = [
    OnboardingModel("assets/image1.png",
        "Too tired to walk your dog? \n            Let’s help you!"),
    OnboardingModel("assets/image1.png",
        "Too tired to walk your cat? \n            Let’s help you!"),
    OnboardingModel("assets/image1.png",
        "Too tired to walk your sheep? \n            Let’s help you!")
  ];

  nextSlide() {
    pageController.nextPage(duration: 300.milliseconds, curve: Curves.easeIn);
  }

  // autoAnimate(index) {
  //   pageViewAutoAnimateTimer = Timer.periodic(5.seconds, (timer) {
  //     if (pageController.page != 2.0) {
  //       pageController.nextPage(
  //           duration: 400.milliseconds, curve: Curves.linear);
  //     }
  //   });
  //   // _pageViewAutoAnimateTimer.cancel();
  // }

  signUp() {
    Get.to(const SignUpScreen());
  }
}
