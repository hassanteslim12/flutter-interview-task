import 'package:get/get.dart';

class NavController extends GetxController{
  int _currentIndex = 0;
  int get currentIndex => _currentIndex;
   bool _reverse = false;
   
  void setIndex(int value) {
    if (value < _currentIndex) {
      _reverse = true;
    } else {
      _reverse = false;
    }
    _currentIndex = value;
    update();
  }

  bool isIndexSelected(int index) => _currentIndex == index;
}