import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SignUpController extends GetxController {
  TextEditingController fullName = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController password = TextEditingController();
  bool isObscured = true;

  void toggleObscurity() {
    isObscured = !isObscured;
    update();
  }

  void onSignupPress() async {}
}
