import 'package:get/get.dart';
import 'package:woo_dog/models/tile_model.dart';

class HomeController extends GetxController {
  List<TileModel> nearWalkers = [
    TileModel('assets/frame1.png', 'Mason York', 7, 3),
    TileModel('assets/frame2.png', 'Mark Greene', 14, 4),
    TileModel('assets/frame1.png', 'Trina Kain', 9, 3),
    TileModel('assets/frame2.png', 'Mason York', 10, 5),
  ];

    List<TileModel> suggestedWalkers = [
    TileModel('assets/frame2.png', 'Mark Greene', 2, 5),
    TileModel('assets/frame1.png', 'Trina Kain', 4, 4),
    TileModel('assets/frame2.png', 'Mason York', 9, 3),
    TileModel('assets/frame1.png', 'Mark Greene', 7, 5),
  ];
}
