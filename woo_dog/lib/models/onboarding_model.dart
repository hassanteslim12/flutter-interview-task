class OnboardingModel{
  final String image;
  final String title;
  OnboardingModel(this.image, this.title);
}