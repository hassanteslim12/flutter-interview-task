class TileModel {
  final String image;
  final String name;
  final int distance;
  final int charges;
  TileModel(this.image, this.name, this.distance, this.charges);
}
