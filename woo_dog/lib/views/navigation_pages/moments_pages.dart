import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:woo_dog/controllers/navigation_pages_controllers/moments_controller.dart';

class MomentsPage extends StatelessWidget {
  const MomentsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MomentsController>(
      init: MomentsController(),
      builder: (controller) {
      return const Scaffold(
        body: Center(child: Text('Moments Page')),
      );
    });
  }
}
