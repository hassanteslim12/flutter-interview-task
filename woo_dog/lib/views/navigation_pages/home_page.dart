import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:woo_dog/constants/size_config.dart';
import 'package:woo_dog/controllers/navigation_pages_controllers/home_controller.dart';
import 'package:woo_dog/views/walker_details.dart';
import 'package:woo_dog/widgets/home_header.dart';
import 'package:woo_dog/widgets/tile_details.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
        init: HomeController(),
        builder: (controller) {
          return Scaffold(
            body: SafeArea(
              child: SingleChildScrollView(
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                  child: Column(
                    children: [
                      HomeHeader(
                          title: 'Home',
                          subtitle: 'Explore dog walkers',
                          titleFont: SizeConfig.fontSize(context, 7),
                          subtitleFont: SizeConfig.fontSize(context, 5),
                          buttonWidth: SizeConfig.fromWidth(context, 30),
                          buttonHeight: SizeConfig.fromHeight(context, 5),
                          buttonColor: const Color.fromRGBO(254, 144, 75, 1)),
                      SizedBox(
                        height: SizeConfig.fromHeight(context, 3),
                      ),
                      TextFormField(
                          decoration: InputDecoration(
                        prefixIcon: const Icon(Icons.add_location),
                        suffixIcon: const Icon(Icons.sort),
                        hintText: 'Kiyv, Ukraine',
                        fillColor: Colors.grey[200],
                        filled: true,
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                            color: Colors.transparent,
                          ),
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                      )),
                      SizedBox(
                        height: SizeConfig.fromHeight(context, 3),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Near you',
                            style: TextStyle(
                                fontSize: SizeConfig.fontSize(context, 7),
                                fontWeight: FontWeight.bold),
                          ),
                          const Text('View all',
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 20))
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.fromHeight(context, 2),
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: SizeConfig.fromHeight(context, 25),
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.nearWalkers.length,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => WalkerDetails(
                                              tileModel:
                                                  controller.nearWalkers[index],
                                            ))),
                                child: Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 25),
                                  child: Column(
                                    children: [
                                      Image.asset(
                                          controller.nearWalkers[index].image,
                                          width: SizeConfig.fromWidth(
                                              context, 60)),
                                      const SizedBox(height: 20),
                                      TileDetails(
                                        titleFont:
                                            SizeConfig.fontSize(context, 4),
                                        subtitleFont:
                                            SizeConfig.fontSize(context, 2),
                                        title:
                                            controller.nearWalkers[index].name,
                                        subtitle:
                                            '${controller.nearWalkers[index].distance}km from you',
                                        buttonText:
                                            '${controller.nearWalkers[index].charges}/hr',
                                      )
                                    ],
                                  ),
                                ),
                              );
                            }),
                      ),
                      SizedBox(
                        height: SizeConfig.fromHeight(context, 2),
                      ),
                      Divider(color: Colors.grey[95], thickness: 2),
                      SizedBox(
                        height: SizeConfig.fromHeight(context, 2),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Suggested',
                            style: TextStyle(
                                fontSize: SizeConfig.fontSize(context, 7),
                                fontWeight: FontWeight.bold),
                          ),
                          const Text('View all',
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  fontSize: 20))
                        ],
                      ),
                      SizedBox(
                        height: SizeConfig.fromHeight(context, 2),
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: SizeConfig.fromHeight(context, 25),
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: controller.suggestedWalkers.length,
                            itemBuilder: (context, index) {
                              return Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 25),
                                child: Column(
                                  children: [
                                    Image.asset(
                                        controller
                                            .suggestedWalkers[index].image,
                                        width:
                                            SizeConfig.fromWidth(context, 60)),
                                    const SizedBox(height: 20),
                                    TileDetails(
                                      titleFont:
                                          SizeConfig.fontSize(context, 4),
                                      subtitleFont:
                                          SizeConfig.fontSize(context, 2),
                                      title: controller
                                          .suggestedWalkers[index].name,
                                      subtitle:
                                          '${controller.suggestedWalkers[index].distance}km from you',
                                      buttonText:
                                          '${controller.suggestedWalkers[index].charges}/h',
                                    )
                                  ],
                                ),
                              );
                            }),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
}
