import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:woo_dog/controllers/navigation_pages_controllers/profile_controller.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileController>(
        init: ProfileController(),
        builder: (controller) {
          return const Scaffold(
            body: Center(child: Text('Profile Page')),
          );
        });
  }
}
