import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:woo_dog/controllers/navigation_pages_controllers/chat_controller.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ChatController>(
      init: ChatController(),
      builder: (controller) {
      return const Scaffold(
        body: Center(child: Text('Chat Page')),
      );
    });
  }
}
