import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:woo_dog/controllers/nav_controller.dart';
import 'package:woo_dog/views/navigation_pages/chat_page.dart';
import 'package:woo_dog/views/navigation_pages/home_page.dart';
import 'package:woo_dog/views/navigation_pages/moments_pages.dart';
import 'package:woo_dog/views/navigation_pages/profile_page.dart';

class Home extends StatelessWidget {
  const Home({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<NavController>(
        init: NavController(),
        builder: (controller) {
          return Scaffold(
            //passing in the current index from the view model
            // so it can return the right screen

            body: getViewForIndex(controller.currentIndex),
            bottomNavigationBar: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              backgroundColor: Colors.white,
              selectedItemColor: Colors.black,
              unselectedItemColor: Colors.grey,
              selectedFontSize: 14,
              unselectedFontSize: 14,
              currentIndex: controller.currentIndex,
              onTap: controller.setIndex,
              items: getBottomIcons(),
            ),
          );
        });
  }
}

List<BottomNavigationBarItem> getBottomIcons() {
  List<String> name = ['Home', 'Moments', 'Chat', 'Profile'];
  List icons = [Icons.home, Icons.people, Icons.send, Icons.person];

  List<BottomNavigationBarItem> bottomNavList = List.generate(4, (i) {
    var item = BottomNavigationBarItem(
      label: name[i],
      icon: Icon(
        icons[i],
      ),
      activeIcon: Icon(
        icons[i],
        color: Colors.black,
      ),
    );

    return item;
  });

  return bottomNavList;
}

Widget getViewForIndex(int index) {
  switch (index) {
    case 0:
      return const HomePage();
    case 1:
      return const MomentsPage();
    // case 2:
    //   return const IntegratePage();
    case 2:
      return const ChatPage();
    case 3:
      return const ProfilePage();
    default:
      return Container();
  }
}
