import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:woo_dog/constants/size_config.dart';
import 'package:woo_dog/controllers/sign_up_controller.dart';
import 'package:woo_dog/views/nav_view.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SignUpController>(
      init: SignUpController(),
      builder: (controller) {
        return Scaffold(
          appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0.0,
              leading: IconButton(
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.black,
                ),
                onPressed: () {
                  Get.back();
                },
              )),
          body: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
            child: SingleChildScrollView(
              child: Form(
                key: _key,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Let's start here",
                      style: TextStyle(
                          fontSize: SizeConfig.fontSize(context, 7),
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: SizeConfig.fromHeight(context, 1),
                    ),
                    Text(
                      "Fill in your details to begin",
                      style: TextStyle(
                          fontSize: SizeConfig.fontSize(context, 5),
                          color: Colors.grey),
                    ),
                    SizedBox(
                      height: SizeConfig.fromHeight(context, 3),
                    ),
                    TextFormField(
                      key: const Key("Full Name"),
                        maxLines: 2,
                        decoration: InputDecoration(
                          labelText: 'Fullname',
                          fillColor: Colors.grey[200],
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Colors.transparent,
                            ),
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        )),
                    SizedBox(
                      height: SizeConfig.fromHeight(context, 3),
                    ),
                    TextFormField(
                       key: const Key("Email"),
                        maxLines: 2,
                        decoration: InputDecoration(
                          labelText: 'E-mail',
                          fillColor: Colors.grey[200],
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Colors.transparent,
                            ),
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        )),
                    SizedBox(
                      height: SizeConfig.fromHeight(context, 3),
                    ),
                    TextFormField(
                       key: const Key("Password"),
                        style: const TextStyle(
                          height: 3.0,
                        ),
                        keyboardType: TextInputType.visiblePassword,
                        onEditingComplete: () =>
                            FocusScope.of(context).nextFocus(),
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        controller: controller.password,
                        obscureText: controller.isObscured,
                        decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              controller.isObscured
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                            onPressed: () => controller.toggleObscurity(),
                          ),
                          labelText: 'Password',
                          fillColor: Colors.grey[200],
                          filled: true,
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                              color: Colors.transparent,
                            ),
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        )),
                    SizedBox(
                      height: SizeConfig.fromHeight(context, 3),
                    ),
                    SizedBox(
                      width: SizeConfig.fromWidth(context, 100),
                      height: SizeConfig.fromHeight(context, 6),
                      child: TextButton(
                         key: const Key("Sign up"),
                        child: const Text('Sign up'),
                        style: TextButton.styleFrom(
                            backgroundColor:
                                const Color.fromRGBO(254, 144, 75, 1),
                            primary: Colors.white,
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            textStyle: TextStyle(
                                fontSize: SizeConfig.fontSize(context, 4),
                                fontWeight: FontWeight.bold)),
                        onPressed: () => Get.to(const Home()),
                      ),
                    ),
                    SizedBox(height: SizeConfig.fromHeight(context, 37)),
                    Center(
                      child: Column(
                        children: [
                          RichText(
                              text: TextSpan(
                                  text: 'By signing in, I agree with',
                                  style: const TextStyle(
                                      color: Colors.grey, fontSize: 20),
                                  children: <TextSpan>[
                                TextSpan(
                                    text: 'Terms Of Use',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        // navigate to desired screen
                                      })
                              ])),
                          RichText(
                              text: TextSpan(
                                  text: 'and ',
                                  style: const TextStyle(
                                      color: Colors.grey, fontSize: 20),
                                  children: <TextSpan>[
                                TextSpan(
                                    text: 'Privacy Policy',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        // navigate to desired screen
                                      })
                              ]))
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
