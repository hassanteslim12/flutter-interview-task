import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:woo_dog/constants/size_config.dart';
import 'package:woo_dog/controllers/walker_details_controller.dart';
import 'package:woo_dog/models/tile_model.dart';
import 'package:woo_dog/views/navigation_pages/chat_page.dart';

class WalkerDetails extends StatelessWidget {
  const WalkerDetails({Key? key, required this.tileModel}) : super(key: key);

  final TileModel tileModel;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<WalkerDetailsController>(
        init: WalkerDetailsController(),
        builder: (controller) {
          return Scaffold(
              body: Stack(
            children: [
              Stack(
                children: [
                  Image.asset(
                    tileModel.image,
                    width: double.infinity,
                    height: SizeConfig.fromHeight(context, 53),
                    fit: BoxFit.cover,
                  ),
                ],
              ),
              Positioned(
                child: Container(
                    padding: const EdgeInsets.all(20),
                    margin: EdgeInsets.only(
                      top: SizeConfig.fromHeight(context, 50),
                    ),
                    height: SizeConfig.fromHeight(context, 60),
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(45),
                          topRight: Radius.circular(45),
                        )),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Center(
                          child: Text(
                            tileModel.name,
                            style: TextStyle(
                                fontSize: SizeConfig.fontSize(context, 7),
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text('\$' '${tileModel.charges}/hr',
                                style: const TextStyle(fontSize: 20)),
                            const VerticalDivider(color: Colors.black),
                            Text('${tileModel.distance}/km',
                                style: const TextStyle(fontSize: 20)),
                            const VerticalDivider(color: Colors.black),
                            Row(
                              children: const [
                                Text('4.4', style: TextStyle(fontSize: 20)),
                                Icon(Icons.star)
                              ],
                            ),
                            const VerticalDivider(color: Colors.black),
                            const Text('450 walks',
                                style: TextStyle(fontSize: 20)),
                          ],
                        ),
                        Divider(color: Colors.grey[90], thickness: 2),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(
                              height: SizeConfig.fromHeight(context, 5),
                              width: SizeConfig.fromHeight(context, 12),
                              child: TextButton(
                                child: const Text('About'),
                                style: TextButton.styleFrom(
                                    backgroundColor: Colors.black,
                                    primary: Colors.white,
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    textStyle: TextStyle(
                                        fontSize:
                                            SizeConfig.fontSize(context, 3),
                                        fontWeight: FontWeight.bold)),
                                onPressed: () {},
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.fromHeight(context, 5),
                              width: SizeConfig.fromHeight(context, 12),
                              child: TextButton(
                                child: const Text('Location'),
                                style: TextButton.styleFrom(
                                    backgroundColor: Colors.black12,
                                    primary: Colors.grey,
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    textStyle: TextStyle(
                                        fontSize:
                                            SizeConfig.fontSize(context, 3),
                                        fontWeight: FontWeight.bold)),
                                onPressed: () {},
                              ),
                            ),
                            SizedBox(
                              height: SizeConfig.fromHeight(context, 5),
                              width: SizeConfig.fromHeight(context, 12),
                              child: TextButton(
                                child: const Text('Reviews'),
                                style: TextButton.styleFrom(
                                    backgroundColor: Colors.black12,
                                    primary: Colors.grey,
                                    shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    textStyle: TextStyle(
                                        fontSize:
                                            SizeConfig.fontSize(context, 3),
                                        fontWeight: FontWeight.bold)),
                                onPressed: () {},
                              ),
                            )
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        Row(children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Age',
                                  style: TextStyle(
                                      fontSize: SizeConfig.fontSize(context, 4),
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey)),
                              Text(
                                "30 years",
                                style: TextStyle(
                                  fontSize: SizeConfig.fontSize(context, 6),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Experience',
                                  style: TextStyle(
                                      fontSize: SizeConfig.fontSize(context, 4),
                                      fontWeight: FontWeight.bold,
                                      color: Colors.grey)),
                              Text(
                                "11 months",
                                style: TextStyle(
                                  fontSize: SizeConfig.fontSize(context, 6),
                                ),
                              ),
                            ],
                          ),
                        ]),
                        const SizedBox(
                          height: 20,
                        ),
                        Text(
                          'Alex has loved dogs since childhood. He is currently a veterinary student. Visits the dog shelter we...',
                          style: TextStyle(
                            fontSize: SizeConfig.fontSize(context, 3.5),
                          ),
                        ),
                        TextButton(
                            onPressed: () {},
                            child: Text('Read More',
                                style: TextStyle(
                                  fontSize: SizeConfig.fontSize(context, 3.5),
                                )),
                            style: TextButton.styleFrom(
                              primary: const Color.fromRGBO(254, 144, 75, 1),
                            )),
                        SizedBox(
                            width: SizeConfig.fromWidth(context, 100),
                            height: SizeConfig.fromHeight(context, 6),
                            child: TextButton(
                              child: const Text('Check schedule '),
                              style: TextButton.styleFrom(
                                  backgroundColor:
                                      const Color.fromRGBO(254, 144, 75, 1),
                                  primary: Colors.white,
                                  shape: const RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  textStyle: TextStyle(
                                      fontSize: SizeConfig.fontSize(context, 4),
                                      fontWeight: FontWeight.bold)),
                              onPressed: () => Get.to(const ChatPage()),
                            )),
                      ],
                    )),
              ),
              InkWell(
                onTap: () => Get.back(),
                child: Container(
                    margin: EdgeInsets.only(
                      top: SizeConfig.fontSize(context, 15),
                      left: SizeConfig.fontSize(context, 5),
                    ),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                      color: Color.fromRGBO(196, 196, 196, 0.4),
                    ),
                    child: Icon(Icons.close,
                        color: Colors.grey,
                        size: SizeConfig.fontSize(context, 10))),
              )
            ],
          ));
        });
  }
}
