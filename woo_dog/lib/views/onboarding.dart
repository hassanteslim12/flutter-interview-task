import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:get/get.dart';
import 'package:woo_dog/constants/size_config.dart';
import 'package:woo_dog/controllers/onboarding_controller.dart';
import 'package:woo_dog/views/sign_up_view.dart';

class OnboardingScreen extends StatelessWidget {
  final _controller = OnboardingController();

  OnboardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            PageView.builder(
                controller: _controller.pageController,
                onPageChanged: _controller.selectedPageindex,
                itemCount: _controller.onboardingItems.length,
                itemBuilder: (context, index) {
                  // _controller.autoAnimate(index);
                  return Container(
                    color: Colors.black87,
                    child: Stack(
                      children: [
                        Image.asset(
                          _controller.onboardingItems[index].image,
                        ),
                        Positioned(
                          bottom: SizeConfig.fontSize(context, 50),
                          left: SizeConfig.fontSize(context, 20),
                          child: Text(
                            _controller.onboardingItems[index].title,
                            style: TextStyle(
                                fontSize: SizeConfig.fontSize(context, 5),
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  );
                }),
            Positioned(child: Image.asset('assets/logo.png')),
            Positioned(
                left: SizeConfig.fontSize(context, 15),
                bottom: SizeConfig.fontSize(context, 30),
                child: SizedBox(
                  width: SizeConfig.fromWidth(context, 70),
                  height: SizeConfig.fromHeight(context, 6),
                  child: TextButton(
                    key: const Key('Join Community'),
                    child: const Text('Join our Community'),
                    style: TextButton.styleFrom(
                        backgroundColor: const Color.fromRGBO(254, 144, 75, 1),
                        primary: Colors.white,
                        shape: const RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        textStyle: TextStyle(
                            fontSize: SizeConfig.fontSize(context, 4),
                            fontWeight: FontWeight.bold)),
                    onPressed: () => Get.to(const SignUpScreen())
                  ),
                )),
            Positioned(
                left: SizeConfig.fontSize(context, 30),
                bottom: SizeConfig.fontSize(context, 20),
                child: Row(
                  children: [
                    const Text('Already a member?',
                        style: TextStyle(color: Colors.white, fontSize: 20)),
                    InkWell(
                      onTap: () {},
                      child: const Text('Sign in',
                          style: TextStyle(color: Color.fromRGBO(254, 144, 75, 1), fontSize: 20)),
                    ),
                  ],
                )),
            Positioned(
                bottom: SizeConfig.fontSize(context, 70),
                left: SizeConfig.fontSize(context, 40),
                child: Row(
                    children: List.generate(
                        _controller.onboardingItems.length,
                        (index) => Obx(() => InkWell(
                              onTap: () {
                                _controller.pageController.jumpToPage(index);
                              },
                              child: Container(
                                margin: EdgeInsets.all(
                                  SizeConfig.fontSize(context, 1),
                                ),
                                width: SizeConfig.fontSize(context, 5),
                                height: SizeConfig.fontSize(context, 5),
                                decoration: BoxDecoration(
                                  color: _controller.selectedPageindex.value ==
                                          index
                                      ? const Color.fromRGBO(254, 144, 75, 1)
                                      : Colors.grey,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                              ),
                            ))))),
          ],
        ),
      ),
    );
  }
}

















          // Padding(
          //             padding: const EdgeInsets.symmetric(horizontal: 10.0),
          //             child: Column(
          //               mainAxisAlignment: MainAxisAlignment.center,
          //               children: [
          //                 Image.asset(
          //                   _controller.onboardingItems[index].image,
          //                   width: SizeConfig.fromHeight(context, 35),
          //                   height: SizeConfig.fromHeight(context, 35),
          //                 ),
          //                 SizedBox(
          //                   height: SizeConfig.fromHeight(context, 4),
          //                 ),
          //                 Text(
          //                   _controller.onboardingItems[index].title,
          //                   style: TextStyle(
          //                       fontSize: SizeConfig.fontSize(context, 5),
          //                       fontWeight: FontWeight.bold),
          //                 ),
          //                 SizedBox(
          //                   height: SizeConfig.fromHeight(context, 4),
          //                 ),
          //                 Text(
          //                   _controller.onboardingItems[index].title,
          //                   style: TextStyle(
          //                     fontSize: SizeConfig.fontSize(context, 4),
          //                   ),
          //                   textAlign: TextAlign.center,
          //                 )
          //               ],
          //             ),
          //           );