import 'package:flutter/material.dart';
import 'package:woo_dog/constants/size_config.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader(
      {Key? key,
      required this.titleFont,
      required this.subtitleFont,
      required this.title,
      required this.subtitle,
      required this.buttonColor,
      required this.buttonWidth,
      required this.buttonHeight})
      : super(key: key);
  final double titleFont;
  final double subtitleFont;
  final String title;
  final String subtitle;
  final Color buttonColor;
  final double buttonWidth;
  final double buttonHeight;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style:
                  TextStyle(fontSize: titleFont, fontWeight: FontWeight.bold),
            ),
            const SizedBox(
              height: 10,
            ),
            Text(
              subtitle,
              style: TextStyle(fontSize: subtitleFont, color: Colors.grey),
            ),
          ],
        ),
        SizedBox(
          width: buttonWidth,
          height: buttonHeight,
          child: TextButton(
            child: Row(
              children: const [
                Icon(Icons.add),
                Text('  Book a walk'),
              ],
            ),
            style: TextButton.styleFrom(
                backgroundColor: buttonColor,
                primary: Colors.white,
                shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                textStyle: TextStyle(
                    fontSize: SizeConfig.fontSize(context, 3),
                    fontWeight: FontWeight.bold)),
            onPressed: () {},
          ),
        ),
      ],
    );
  }
}
