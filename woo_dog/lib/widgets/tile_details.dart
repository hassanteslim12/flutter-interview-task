import 'package:flutter/material.dart';
import 'package:woo_dog/constants/size_config.dart';

class TileDetails extends StatelessWidget {
  const TileDetails({
    Key? key,
    required this.titleFont,
    required this.subtitleFont,
    required this.title,
    required this.subtitle,
    required this.buttonText,
  }) : super(key: key);
  final double titleFont;
  final double subtitleFont;
  final String title;
  final String subtitle;
  final String buttonText;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 340,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style:
                    TextStyle(fontSize: titleFont, fontWeight: FontWeight.bold),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                subtitle,
                style: TextStyle(fontSize: subtitleFont, color: Colors.grey),
              ),
            ],
          ),
          SizedBox(
            width: SizeConfig.fontSize(context, 12),
            height: SizeConfig.fontSize(context, 8),
            child: TextButton(
              child: Text('\$' '$buttonText'),
              style: TextButton.styleFrom(
                  backgroundColor: Colors.black,
                  primary: Colors.white,
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  textStyle: TextStyle(
                      fontSize: SizeConfig.fontSize(context, 3),
                      fontWeight: FontWeight.bold)),
              onPressed: () {},
            ),
          ),
        ],
      ),
    );
  }
}
